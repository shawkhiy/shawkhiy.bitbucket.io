# If you are regularly crawling WordPress.com sites, please use our firehose to receive real-time push updates instead.
# Please see https://developer.wordpress.com/docs/firehose/ for more details.

User-agent: *
Disallow: /account/
User-agent: *
Disallow: /wp-admin/
Allow: /wp-admin/admin-ajax.php
Sitemap: http://variety.com/news-sitemap.xml
Sitemap: http://variety.com/sitemap_index.xml
Disallow: /wp-login.php
Disallow: /activate/ # har har
Disallow: /cgi-bin/ # MT refugees
Disallow: /mshots/v1/
Disallow: /next/
Disallow: /public.api/

User-agent: IRLbot
Crawl-delay: 3600

# This file was generated on Wed, 06 Dec 2017 22:05:17 +0000
User-agent: *
Disallow: /?s=
Disallow: /*/?s=
Disallow: /search/
Disallow: /search?
Disallow: *?v02
Disallow: *?replytocom
User-agent: *
Disallow: /*preview=true
Disallow: /*theme_preview=true
Disallow: /tips/ 
Disallow: /ces_one/ 
Disallow: /2015/tv/news/jessica-jones-mike-colter-luke-cage-netflix-1201843290/ 
Disallow: /2015/tv/news/lamar-odom-coma-awakens-breathing-1201620175/ 
Disallow: /premier-conferences-registration/ 
Disallow: /premier-archives-registration/ 
Disallow: /comments_app/ 
Disallow: /results/ 
Disallow: /genre/
Disallow: /genre/*
